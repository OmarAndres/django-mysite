$(function()
{
	let numeros = '1234567890';
	let letras	= 'qwertyuiopasdfghjklñzxcvbnm' +
				  'QWERTYUIOPASDFGHJKLÑZXCVBNM' +
				  'áéíóúÁÉÍÓÚ';
	let email 	= /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/ ;			  			  
	
	
	$('.soloNumeros').keypress(function(e){
		let caracter = String.fromCharCode(e.which);
		
		if(numeros.indexOf(caracter)<0)
				return false;
	});

	$('.CV').keypress(function(e){
		let caracter = String.fromCharCode(e.which);
		let patron = numeros + "kK";
		if(patron.indexOf(caracter) < 0)
				return false;
	});		

	$('.soloLetras').keypress(function(e){
		let caracter = String.fromCharCode(e.which);
		let patron = letras + "' ";

		if(patron.indexOf(caracter)<0)
				return false;
	});			  

	$('.dirección').keypress(function(e){	
		let caracter = String.fromCharCode(e.which);
		let patron = letras + "'#- ";
		if(patron.indexOf(caracter) < 0)
				return false;
	});		

	$('.correo').keypress(function(e){	
		let caracter = String.fromCharCode(e.which);
		let patron = 'qwertyuiopasdfghjklzxcvbnm' + 'QWERTYUIOPASDFGHJKLZXCVBNM' + numeros + "'#-@_. ";
		if(patron.indexOf(caracter) < 0)
				return false;
	});	

	$('.clave').keypress(function(e){	
		let caracter = String.fromCharCode(e.which);
		let patron = letras + numeros + "'#-@_!#& ";
		if(patron.indexOf(caracter) < 0)
				return false;
	});

	$('.btnAtras').click(function()
	{
			
		window.location.href="Inicio.html";
			
	});

	$('.btnLimpiar').click(function()
	{
		$('input[type=text],input[type=date]').val('');		  
		$('select').val('0');
		$('input[type=text]:first').focus();
	});

	$('.btnRegistrar').click(function()
	{
		let rut = $('.txtRut').val();
		let cv = $('.txtCV').val(); 

		if($('.txtEmail').val() == '')
		{
			alert('Ingrese su Correo Electronico.');
			$('.txtEmail').focus();
		}
		else if(!email.test($('.txtEmail').val()))
		{
			alert('Formato del Correo no coincide. Especifique un Correo Electronico valido.');
			$('.txtEmail').focus();
		}
		else if(rut.length == '0')
		{
			alert('Ingrese su Rut.');
			$('.txtRut').focus();
		}
		else if(cv.length == '0')
		{
			alert('Ingrese Codigo Verificador correspondiente a su Rut.');
			$('.txtCV').focus();
		}
		else if(!esValidoElRut($('.txtRut').val(),$('.txtCV').val()))
		{
			alert('El rut no es válido');
			$('.txtRut,.txtCV').val('');	
			$('.txtRut').focus();
		}
		else if($('.txtNombre').val() == '')
		{
			alert('Ingrese su Nombre.');
			$('.txtNombre').focus();
		}
		else if($('.txtNac').val() == '')
		{
			alert('Ingrese su Fecha de Nacimiento.');
			$('.txtNac').focus();
		}
		else if($('.txtNac').val() >= "2001-01-01")
		{
    		alert('Fecha de Nacimiento invalida. Debes ser mayor de edad para postular a adopción.');
		}
		else if($('.txtTelefono').val() == '')
		{
			alert('Ingrese su numero teléfonico.');
			$('.txtTelefono').focus();
		}
		else if($('.cmbRegion').val() == '0')
		{
			alert('Seleccione su Región de residencia.');
			$('.cmbRegion').focus();
		}
		else if($('.cmbCiudad').val() == '0')
		{
			alert('Seleccione su Ciudad de residencia.');
			$('.cmbCiudad').focus();
		}
		else if($('.cmbNE').val() == '0')
		{
			alert('Seleccione su Nivel Educacional.');
			$('.cmbNE').focus();
		}
		else 
		{
			alert("Postulación enviada, Nos pondremos en contacto contigo a la brevedad. !GRACIAS POR POSTULAR!");	
				$('input[type=text],input[type=date]').val('');		  
				$('select').val('0');
				window.location.href="Inicio.html";
		}	
	});

	function esValidoElRut(Rut,Digito)
    {
        Digito = Digito.toUpperCase();
		var longitud        = Rut.length;
		var factor          = 2;
		var sumaProducto    = 0;
		var con             = 0;
		var caracter     	= 0;
 
		for( con=longitud-1; con>=0; con--)
		{
			caracter = Rut.charAt(con);
			sumaProducto += (factor * caracter);
			if (++factor > 7)
				factor=2;		
		}
 
        var digitoAuxiliar	= 11-(sumaProducto % 11);   
        var caracteres		= "-123456789K0";
        var digitoCaracter= caracteres.charAt(digitoAuxiliar);
        return digitoCaracter == Digito ? 1 : 0;            
    }    
});

