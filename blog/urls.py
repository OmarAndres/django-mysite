from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.Inicio),
	url(r'Tienda', views.Tienda),
	url(r'Adopcion', views.Adopción),
	]