# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-01-29 23:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_producto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='Imagen',
            field=models.ImageField(max_length=200, upload_to=''),
        ),
    ]
